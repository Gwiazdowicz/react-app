import React from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import { Nav } from  'react-bootstrap';
import HomePage from './HomePage/HomePage';
import PaddleGame from './PaddleGame/PaddleGame';
import TicTacToe from './TicTacToe/TicTacToe';
import Settings from './Settings/Settings';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import { faHome } from "@fortawesome/free-solid-svg-icons";
import { faTimesCircle} from '@fortawesome/free-regular-svg-icons';
import { faArrowsAlt } from '@fortawesome/free-solid-svg-icons';
import { faCog } from '@fortawesome/free-solid-svg-icons';
import lang from './lang.json'; 
import './App.css';
import { EventEmitter } from './EventEmitter';

class App extends React.Component {
  constructor(){
    super();

    this.state = {
      appRefresh: null,
    }

    this.state={
      appRefresh:null,
    }

    if(!localStorage.getItem("lang")){
      localStorage.setItem('lang','en');
    }

    EventEmitter.subscribe('langChange', () => {
      this.setState({
        settingsChangedOn: new Date()
      })
    })
  }
  
  render(){
    return (
      <>
        <Router>
          <div className="container"> 
            <Nav  defaultActiveKey="/home" >
              <Nav.Item>
                <Link className="nav-link"  to="/"><p>{lang[localStorage.getItem('lang')].navstart} <FontAwesomeIcon icon={faHome} /></p></Link>
              </Nav.Item>
              <Nav.Item>
                <Link className="nav-link" to="/tictactoe"><p>{lang[localStorage.getItem('lang')].tictactoe} <FontAwesomeIcon icon={faTimesCircle} /></p> </Link>
              </Nav.Item>
              <Nav.Item>
                <Link className="nav-link" to="/paddlegame"><p>{lang[localStorage.getItem('lang')].paddlegame} <FontAwesomeIcon icon={faArrowsAlt} /></p> </Link>
              </Nav.Item>
              <Nav.Item>
                <Link className="nav-link" to="/settings"><p>{lang[localStorage.getItem('lang')].settings} <FontAwesomeIcon icon={faCog} /></p> </Link>
              </Nav.Item>
            </Nav>
          </div>
          <div className="container">
            <Route exact path = "/" component={HomePage} />
            <Route path = "/tictactoe" component={TicTacToe} />
            <Route path = "/paddlegame" component={PaddleGame} />
            <Route path = "/settings" component={Settings} />
          </div>
        </Router>
      </>
    );
  }
}
export default App;
 