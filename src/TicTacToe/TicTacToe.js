import React from 'react';
import './TicTacToe.css';
import { Button } from  'react-bootstrap';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import { faTrophy }from '@fortawesome/free-solid-svg-icons';
import lang from '../lang.json'; 


class TicTacToe extends React.Component {
  constructor(){
    super();

    this.state = {
      player1:  "O",
      player2:  "X",
      turn: 0 , 
      isModalVisible: false,
      winner: "",
      board: [
      '','','',
      '','','',
      '','','',
      ],
      gameEnabled: true,
      Intervalid:null,
      }
      this.computerTurn = this.computerTurn.bind(this);
    }

  checkGameStatus(selectedPlayer){
    if (!this.state.gameEnabled) { return }

      for(let i = 0; i <= 6; i= i+3 ){
        if(!!this.state.board[i] && !!this.state.board[i+1] && !!this.state.board[i+2]) {
          if (this.state.board[i] === this.state.board[i+1] && this.state.board[i+1] === this.state.board[i+2]){
            this.endGame(selectedPlayer);
            console.log(selectedPlayer);
            return;
          }
        }
      }

    for (let i = 0; i < 3; i++){
      if(!!this.state.board[i] && !!this.state.board[i+3] && !!this.state.board[i+6]){
        if(this.state.board[i] === this.state.board[i+3] && this.state.board[i+3] === this.state.board[i+6]){
          this.endGame(selectedPlayer);
          console.log(selectedPlayer);
          return;
        }
      }
    }

    if(!!this.state.board[0] && !!this.state.board[4] && !!this.state.board[8]){
      if(this.state.board[0] === this.state.board[4] && this.state.board[4] === this.state.board[8]){
        this.endGame(selectedPlayer);
        console.log(selectedPlayer);
        return;
      } 
    }

    if(!!this.state.board[2] && !!this.state.board[4] && !!this.state.board[6]){
      if(this.state.board[2] === this.state.board[4] && this.state.board[4] === this.state.board[6]){
        this.endGame(selectedPlayer);
        console.log(selectedPlayer);
        return;
      }
    }

    if(this.state.gameEnabled === true && this.state.turn === 8){
      this.endGame(false);
      console.log("draw");
    }

  }

  getRandomInt() {
    let min = Math.ceil(0);
    let max = Math.floor(8);

    let randomInt = Math.floor(Math.random() * (max - min + 1)) + min;

    while (this.state.board[randomInt] !== '') {
      randomInt = Math.floor(Math.random() * (max - min + 1)) + min;
    }

    return randomInt;
  }

  computerTurn(){
    let board = this.state.board;

    function _getRandomInt(){
      let min = Math.ceil(0);
      let max = Math.floor(8);

      return Math.floor(Math.random() * (max - min -1)) + min;
    }

    let computerFieldSelected = _getRandomInt();
  
    if(board[computerFieldSelected] === ''){
      board[computerFieldSelected] = 'O'
      this.setState({
        turn: this.state.turn + 1,
        board 
      })

    this.checkGameStatus("O")

    } else if( this.state.gameEnabled && this.state.board.indexOf("") >= 0){
      this.computerTurn();
      return;
    } else return;
  }

  async onFieldClick(index){
    if(!this.state.gameEnabled){ return };
    if(this.state.board[index] !== ''){ alert("To miejsce jest już zajęte"); return };
    if(this.state.turn===0 || this.state.turn===2 || this.state.turn===4 || this.state.turn===6 || this.state.turn===8){
    let board = this.state.board;
    board[index] = "X";
    
    this.setState({
      turn: this.state.turn + 1,
      board
    },this.computerTurn)
    this.checkGameStatus("X");
  }
  }

resetGameBoard(){
  this.setState({
    board:[
      '','','',
      '','','',
      '','','',
    ],
    turn: 0,
    gameEnabled: true,
    isModalVisible: false,
    winner:"",
  })
}

endGame(selectedPlayer){
  if(!selectedPlayer){
    this.setState({
      winner: "Draw",
    })
    this.displayDraw();
  }
  else
  this.setState({
    gameEnabled: false,
    isModalVisible: true,
    winner: selectedPlayer,
  })
  this.displayScore();
}

displayDraw =() =>{
  if(this.state.winner === "Draw"){
    return <h2>{lang[localStorage.getItem('lang')].draw}</h2>; 
  }
  else{
    return;
  }
}

displayScore =(test) =>{
  if(this.state.isModalVisible === true){
    return <h2>{lang[localStorage.getItem('lang')].tictactoewin} : {test}</h2>; 
  }
  else{
    return;
  }
}

  render(){
    return (
      <>
        <h1 className="head-title">{lang[localStorage.getItem('lang')].tictactoe}</h1>
        <div className= {this.state.isModalVisible === true ? "game-board-win" : " game-board" }>
          {this.state.board.map((field, key) => {
          return (
          <div className={field.length > 0 ? "game-board--field container game-board--field--disabled" : "game-board--field container"} key={key} onClick= {this.onFieldClick.bind(this, key)}>
            <div className="game-board--field-content">{field} </div>
          </div>)}) }
          <div className='score-box container' >
          {this.displayScore(this.state.winner)}
          {this.displayDraw()}
          <FontAwesomeIcon icon={faTrophy} className={this.state.isModalVisible === true ? "win-logo" : " win-logo-non" }/>
          </div>
          {this.scoreSystem}
          <Button onClick={this.resetGameBoard.bind(this)} className= " btn btn-info">{lang[localStorage.getItem('lang')].reset}</Button>
        </div>
      </>
    );
  }
}

export default TicTacToe;
 