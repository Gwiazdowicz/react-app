import React from 'react';
import './HomePage.css';
import lang from '../lang.json'; 

class HomePage extends React.Component {
  render(){ 
    return(
      <div>
        <h1 className="head-title">{lang[localStorage.getItem('lang')].title}</h1> 
      </div>
    )
  }
}
export default HomePage;
 