import React from 'react';
import './Settings.css';
import lang from '../lang.json'; 
import { faFlagUsa} from "@fortawesome/free-solid-svg-icons";
import { faGlassWhiskey} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import { EventEmitter } from '../EventEmitter';
import { ButtonGroup } from  'react-bootstrap';
import {Button } from  'react-bootstrap';


class Settings extends React.Component {

  setLanguage(lang){
    localStorage.setItem("lang", lang);
    //window.location.reload();
    EventEmitter.dispatch("langChange", lang);
  }

  setLevel(speed){
    sessionStorage.setItem("speed", speed);
  }

  render(){ 
    return(
      <div>
        <h1 className="head-title">{lang[localStorage.getItem('lang')].settings}</h1>
        <h3>{lang[localStorage.getItem('lang')].selectlan}</h3>
        <div className="btn-lang">
        <button onClick={this.setLanguage.bind(this, 'pl')} className="btn btn-light"><FontAwesomeIcon icon={faGlassWhiskey} /> PL</button>
        <button onClick={this.setLanguage.bind(this, 'en')}  className="btn btn-danger"><FontAwesomeIcon icon={faFlagUsa} /> EN</button>  
        </div>
        <h3>{lang[localStorage.getItem('lang')].levelpaddel}</h3>
        <div className="btn-lang"> 
        <ButtonGroup aria-label="Basic example">
          <Button onClick={this.setLevel.bind(this, 'easy')} variant="success">Easy</Button>
          <Button onClick={this.setLevel.bind(this, 'medium')} variant="primary">Medium</Button>
          <Button onClick={this.setLevel.bind(this, 'hard')}variant="dark">Hard</Button>
        </ButtonGroup>
        </div>
       
        </div>
      
    )
  }
}


export default Settings;
 